﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour
{
    public float visionRadius;
    public float attackRadius;
    public float speed;

    [Tooltip("Prefab de la roca que se disparará")]
    public GameObject rockPrefab;
    [Tooltip("Velocidad de ataque (segundos entre ataques)")]
    public float attackSpeed = 2f;
    bool attacking;

    [Tooltip("Puntos de vida")]
    public int maxHP = 3;
    [Tooltip("Vida actual")]
    public int hp;

    GameObject player;

    Vector3 initialPosition, target;

    Animator anim;
    Rigidbody2D rb2d;

    void Awake()
    {
        hp = maxHP;
    }



    // Start is called before the first frame update
    void Start()
    {
        player = GameObject.FindGameObjectWithTag("Player");

        initialPosition = transform.position;

        anim = GetComponent<Animator>();
        rb2d = GetComponent<Rigidbody2D>();

        //hp = maxHP;
    }

    // Update is called once per frame
    void Update()
    {

        Vector3 target = initialPosition;

        RaycastHit2D hit = Physics2D.Raycast(
            transform.position,
            player.transform.position - transform.position,
            visionRadius,
            1 << LayerMask.NameToLayer("Default"));

        Vector3 forward = transform.TransformDirection(player.transform.position - transform.position);
        Debug.DrawRay(transform.position, forward, Color.red);

        if (hit.collider != null)
        {
            if (hit.collider.tag == "Player")
            {
                target = player.transform.position;
            }
        }

        float distance = Vector3.Distance(target, transform.position);
        Vector3 dir = (target - transform.position).normalized;

        if (target != initialPosition && distance < attackRadius)
        {
            anim.SetFloat("movX", dir.x);
            anim.SetFloat("movY", dir.y);
            anim.Play("Tronco_Run", -1, 0);

            if (!attacking) StartCoroutine(Attack(attackSpeed));
        }

        else
        {
            rb2d.MovePosition(transform.position + dir * speed * Time.deltaTime);

            anim.speed = 1;
            anim.SetFloat("movX", dir.x);
            anim.SetFloat("movY", dir.y);
            anim.SetBool("walking", true);
        }

        if (target == initialPosition && distance < 0.02f)
        {
            transform.position = initialPosition;

            anim.SetBool("walking", false);
        }

        Debug.DrawLine(transform.position, target, Color.green);

    }

    void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.black;
        Gizmos.DrawWireSphere(transform.position, visionRadius);
        Gizmos.DrawWireSphere(transform.position, attackRadius);
    }

    IEnumerator Attack(float seconds)
    {
        attacking = true;
            
        if (target != initialPosition && rockPrefab != null)
        {
            Instantiate(rockPrefab, transform.position, transform.rotation);
            yield return new WaitForSeconds(seconds);

        }
        attacking = false;
    }

    public void Attacked()
    {
        if (--hp <= 0) Destroy(gameObject);
    }

}
