﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class MovimientoPersonaje : MonoBehaviour
{
    protected Joystick joystick;
    protected Joybutton joybutton;
    public SpriteRenderer sprite;
    //public SpriteRenderer arma;
    Animator anim;
    Vector2 mov;

    public lives vida_canvas;
    public int vidas;
    

    public bool golpe;

    CircleCollider2D attackCollider;

    // Start is called before the first frame update
    void Start()
    {
        vida_canvas = GameObject.FindObjectOfType<lives>();
        joystick = FindObjectOfType<Joystick>();
        joybutton = FindObjectOfType<Joybutton>();

        anim = GetComponent<Animator>();

        attackCollider = transform.GetChild(0).GetComponent<CircleCollider2D>();

        attackCollider.enabled = false;
        vidas = 6;

    }

    // Update is called once per frame
    void Update()
    {
        attackCollider.enabled = false;

        mov = new Vector2(joystick.Horizontal, joystick.Vertical);

        var rigidbody = GetComponent<Rigidbody2D>();

        rigidbody.velocity = new Vector3(joystick.Horizontal * 5f, joystick.Vertical * 5f);

        if (joystick.Horizontal > 0)
        {
            sprite.flipX = false;           

            anim.SetFloat("movX", 0);
            anim.SetBool("run", true);

            if (mov != Vector2.zero) attackCollider.offset = new Vector2(0.32f, 0.04f);
        } 
      
        if (joystick.Horizontal < 0)
        {
            sprite.flipX = true;
 
            anim.SetFloat("movX", 1);
            anim.SetBool("run", true);

            if (mov != Vector2.zero) attackCollider.offset = new Vector2(-2f, 0.04f);

        } 

        if (joystick.Horizontal == 0)
        {
            anim.SetBool("run", false);
        }

        if (joybutton.Pressed)
        {
            anim.SetBool("hitting", true);
        } 
        else
        {
            anim.SetBool("hitting", false);
            
        }

        AnimatorStateInfo stateinfo = anim.GetCurrentAnimatorStateInfo(0);
        bool attacking = stateinfo.IsName("Knight_Attack");
         
        if (attacking)
        {
            float playbackTime = stateinfo.normalizedTime;
            if (playbackTime > 0.33) attackCollider.enabled = true;
            else attackCollider.enabled = false;

        }

    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.tag == "bullet")
        {
            if (vidas > 1)
            {
                vidas = vidas - 1;
                vida_canvas.CambioVida(vidas);
                Destroy(collision.gameObject);
            }

            else if (vidas == 1)
            {
                vida_canvas.CambioVida(vidas);
                vidas = vidas - 1;
                Destroy(collision.gameObject);
                //Destroy(gameObject);

                SceneManager.LoadScene("YoudiedScene", LoadSceneMode.Single);
            }

            
        }
    }

    
}
