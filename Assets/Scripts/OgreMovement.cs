﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OgreMovement : MonoBehaviour
{
    private int X;
    private float miposicionX;
    public float speed, limitder, limitizq;
    public SpriteRenderer sprite;

    // Start is called before the first frame update
    void Start()
    {

        X = 1;
        miposicionX = transform.position.x;


    }

    // Update is called once per frame
    void Update()
    {
        transform.Translate(new Vector3(X, 0, 0) * Time.deltaTime * speed, Space.World);

        if (transform.position.x > miposicionX + limitder)
        {
            X = -1;
            sprite.flipX = true;
        }
        if (transform.position.x < miposicionX - limitizq)
        {
            X = 1;
            sprite.flipX = false;

        }

    }
}
