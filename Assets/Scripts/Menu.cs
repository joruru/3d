﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Menu : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void botonExit(){
    
        SceneManager.LoadScene("Menu", LoadSceneMode.Single);
    }

    public void botonJugar() {
    
        SceneManager.LoadScene("SampleScene" , LoadSceneMode.Single);
    
    
    }

    public void botonSalir(){
        
        Application.Quit();
      
    }

    public void botonGraficosUltra(){
    
        QualitySettings.SetQualityLevel(5, true);
    
    }

    public void botonGraficosAltos(){
    
        QualitySettings.SetQualityLevel(4, true);
    
    }

    public void botonGraficosMedios(){
    
        QualitySettings.SetQualityLevel(3, true);
    
    }

    public void botonGraficosBajos(){
    
        QualitySettings.SetQualityLevel(2, true);
    
    }

}
