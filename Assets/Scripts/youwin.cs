﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class youwin : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void onTriggerEnter2D(Collision2D other){
    
        if (other.gameObject.tag == "Player"){
        
            SceneManager.LoadScene("YouwinScene", LoadSceneMode.Single);
        
        }
    
    }

}
