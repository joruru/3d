﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class Joybutton : MonoBehaviour, IPointerUpHandler, IPointerDownHandler
{
    public bool Pressed;

    

    public void OnPointerDown(PointerEventData evenData)
    {
        Pressed = true;
    }

    public void OnPointerUp(PointerEventData evenData)
    {
        Pressed = false;
    }
}
